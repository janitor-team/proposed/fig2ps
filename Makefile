# Directories:
PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man/
MAN1DIR=$(MANDIR)/man1
ETCDIR=/etc/fig2ps
HOMEBINDIR=$(HOME)/bin

# Programs:
MKPATH=mkdir -p
INSTALL=install -m 755 
INSTALLDATA=install -m 644 
LNS=ln -s 
RMF=rm -f
GZIP_FILTER=gzip --best -c 


build: check

check:
	perl -c -w bin/fig2ps

install: build
# Executable file:
	$(MKPATH) $(DESTDIR)$(BINDIR)
	$(INSTALL) bin/fig2ps $(DESTDIR)$(BINDIR)

# Configuration file
	$(MKPATH) $(DESTDIR)$(ETCDIR)
	$(INSTALLDATA) fig2ps.rc $(DESTDIR)$(ETCDIR)

# Manual page
	$(MKPATH) $(DESTDIR)$(MAN1DIR)
	$(GZIP_FILTER) doc/fig2ps.1 > $(DESTDIR)$(MAN1DIR)/fig2ps.1.gz

# Links:
	$(LNS) fig2ps $(DESTDIR)$(BINDIR)/fig2pdf
	$(LNS) fig2ps $(DESTDIR)$(BINDIR)/fig2eps
	$(LNS) fig2ps.1.gz $(DESTDIR)$(MAN1DIR)/fig2pdf.1.gz
	$(LNS) fig2ps.1.gz $(DESTDIR)$(MAN1DIR)/fig2eps.1.gz

install-home: build
	$(MKPATH) $(HOMEBINDIR)
	$(INSTALL) bin/fig2ps $(HOMEBINDIR)

# Do not fail if links exist
	[ -L $(HOMEBINDIR)/fig2pdf ] || $(LNS) fig2ps $(HOMEBINDIR)/fig2pdf
	[ -L $(HOMEBINDIR)/fig2eps ] || $(LNS) fig2ps $(HOMEBINDIR)/fig2eps
	[ -r $(HOME)/.fig2ps.rc ] && \
	echo "Personal configuration file found: not installing a new configuration file" || \
	cp fig2ps.rc $(HOME)/.fig2ps.rc

clean:
	$(RMF) `find . -name "*~"`

